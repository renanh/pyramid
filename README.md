# Hell Triangle - Challenge for B2W Digital#

## The problem: ##


```
Given a triangle of numbers, find the maximum total from top to bottom Example: 

												   6 
												  3 5 
												 9 7 1 
												4 6 8 4    
												

In this triangle the maximum total is 6 + 5 + 7 + 8 = 26 

An element can only be summed with one of the two nearest elements in the next row So the element 3 in row 2 can be summed with 9 and 7, but not with 1 

Choose the programming language you want and let us know about why is that your choice Besides the solution itself, write an automated test for your code (using a known framework or just another function/method) 

Your code will receive an (multidimensional) array as input. The triangle from above would be: 

example = [[6],[3,5],[9,7,1],[4,6,8,4]] 

We are interested in your solution considering: 

1. Correctness
2. Readability
3. Automated Tests
4. Execution Time
 
```

## About the program ##

The Pyramid.java class contains the main method which is expecting a 2D array of integers in the proposed format: "[[6],[3,5],[9,7,1],[4,6,8,4]]".
It also accepts { and } in the array string.

Once started there is one single output, which is the Maximum Total expected.
To get a more verbose output change the log level to Debug and it will print the calculation in this format: "6 + 5 + 7 + 8 = 26".

### Limitations ###
1. The input expects arrays of Integers only;
2. The output supports Long numbers.

### Automated tests ###
This program was developed using TDD. So there is one test class for each class in the application (except for the Exception class).
There is one disabled calculation test because it can hang your computer. It creates a very big pyramid array to test the algorithm for lots of data. But data generation will take a while and depending on the pyramid depth java won't run it till the end. In order to run it I recommend starting with depth = 1000 and then increasing it gradually. There is also a timing output which shows how long it takes to generate and process data.
