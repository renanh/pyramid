package com.b2wdigital.pyramid.core;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.b2wdigital.pyramid.exception.BadPyramidException;

import java.math.BigDecimal;
import java.util.Random;

import static org.junit.Assert.assertTrue;

/**
 * @author Renanh Lima
 * @version 1.0
 * @since 2018-02-16
 */
@RunWith(JUnit4.class)
public class PyramidTest {

	private final static Integer[][] PYRAMYD_BAD_SCENARIO = { { 6 }, { 3, 5 }, { 9, 7, 1 }, { 4, 6, 8, 4, 2 } };
	private final static Integer[][] PYRAMYD_SCENARIO_01 = { { 6 }, { 3, 5 }, { 9, 7, 1 }, { 4, 6, 8, 4 } };
	private final static Integer[][] PYRAMYD_SCENARIO_02 = { { 3 } };
	private final static Integer[][] PYRAMYD_SCENARIO_03 = { { 6 }, { 5, 3 }, { 7, 1, 9 }, { 6, 8, 4, 4 } };
	private final static Integer[][] PYRAMYD_SCENARIO_04 = { { 6 }, { 3, 3 }, { 9, 9, 1 }, { 4, 4, 8, 4 } };

	@Test(expected = NullPointerException.class)
	public void testNullPyramidArray() throws Throwable {
		final Pyramid pyramid = new Pyramid(null);
		pyramid.validatePyramid();
	}

	@Test(expected = BadPyramidException.class)
	public void testWrongPyramidArrayDimension() throws Throwable {
		final Pyramid pyramid = new Pyramid(PYRAMYD_BAD_SCENARIO);
		pyramid.validatePyramid();
	}

	@Test
	public void testValidateInputData() throws Throwable {
		final Pyramid pyramid = new Pyramid(PYRAMYD_SCENARIO_01);
		assertTrue("Pyramid is invalid", pyramid.validatePyramid());
		assertTrue("Pyramid length invalid", pyramid.getPyramidArray().length == 4);
	}

	@Test
	public void testMaximumTotalCalculationScenario01() throws Throwable {
		final Pyramid pyramid = new Pyramid(PYRAMYD_SCENARIO_01);
		final int expectedResult = 26;
		final long gotResult = pyramid.getMaximumTotal();
		assertTrue("Pyramid maximum total is wrong - expected: " + expectedResult + "   got: " + gotResult,
				gotResult == expectedResult);
	}

	@Test
	public void testMaximumTotalCalculationScenario02() throws Throwable {
		final Pyramid pyramid = new Pyramid(PYRAMYD_SCENARIO_02);
		final int expectedResult = 3;
		final long gotResult = pyramid.getMaximumTotal();
		assertTrue("Pyramid maximum total is wrong - expected: " + expectedResult + "   got: " + gotResult,
				gotResult == expectedResult);
	}

	@Test
	public void testMaximumTotalCalculationScenario03() throws Throwable {
		final Pyramid pyramid = new Pyramid(PYRAMYD_SCENARIO_03);
		final int expectedResult = 26;
		final long gotResult = pyramid.getMaximumTotal();
		assertTrue("Pyramid maximum total is wrong - expected: " + expectedResult + "   got: " + gotResult,
				gotResult == expectedResult);
	}

	@Test
	public void testMaximumTotalCalculationScenario04() throws Throwable {
		final Pyramid pyramid = new Pyramid(PYRAMYD_SCENARIO_04);
		final int expectedResult = 22;
		final long gotResult = pyramid.getMaximumTotal();
		assertTrue("Pyramid maximum total is wrong - expected: " + expectedResult + "   got: " + gotResult,
				gotResult == expectedResult);
	}

	@Test
	// @Ignore("This test may hang due to memory consumption. You may want to tweak
	// the pyramid depth before running it.")
	public void testMaximumTotalCalculationBigRandomArray() throws Throwable {
		final Random randomGenerator = new Random();
		final int depth = 10 * 1000;

		final Integer[][] pyramidArray = new Integer[depth][];
		long expectedResult;
		BigDecimal sum = BigDecimal.ZERO;
		int x;
		int y;
		int index = 0;
		int randomInt = 0;
		Integer[] row;

		long startTime = System.currentTimeMillis();
		for (int i = 0; i < depth; ++i) {
			row = new Integer[i + 1];
			for (int j = 0; j <= i; j++) {
				randomInt = randomGenerator.nextInt(Integer.MAX_VALUE);
				row[j] = randomInt;
			}
			if (row.length > 1) {
				x = row[index];
				y = row[index + 1];
				if (y > x) {
					sum = sum.add(new BigDecimal(y));
					index++;
				} else {
					sum = sum.add(new BigDecimal(x));
				}
			} else {
				sum = sum.add(new BigDecimal(randomInt));
			}
			pyramidArray[i] = row;
		}
		long endTime = System.currentTimeMillis();
		long duration = (endTime - startTime);
		System.out.println("Data generation time: " + millisToShortDHMS(duration));

		expectedResult = sum.longValue();

		startTime = System.currentTimeMillis();
		final Pyramid pyramid = new Pyramid(pyramidArray);
		final long gotResult = pyramid.getMaximumTotal();
		endTime = System.currentTimeMillis();
		duration = (endTime - startTime);
		System.out.println("Data processing time: " + millisToShortDHMS(duration));
		assertTrue("Pyramid maximum total is wrong - expected: " + expectedResult + "   got: " + gotResult,
				gotResult == expectedResult);
	}

	/**
	 * converts time (in milliseconds) to human-readable format "<dd:>hh:mm:ss"
	 */
	public static String millisToShortDHMS(long duration) {
		String res = "";
		duration /= 1000;
		int seconds = (int) (duration % 60);
		duration /= 60;
		int minutes = (int) (duration % 60);
		duration /= 60;
		int hours = (int) (duration % 24);
		int days = (int) (duration / 24);
		if (days == 0) {
			res = String.format("%02d:%02d:%02d", hours, minutes, seconds);
		} else {
			res = String.format("%dd%02d:%02d:%02d", days, hours, minutes, seconds);
		}
		return res;
	}
}
