package com.b2wdigital.pyramid.exception;

/**
 * @author Renanh Lima
 * @version 1.0
 * @since 2018-02-16
 */
public class BadPyramidException extends Exception {

	private static final long serialVersionUID = 6657470520781774839L;

	public BadPyramidException() {
	}

	public BadPyramidException(String message) {
		super(message);
	}

	public BadPyramidException(Throwable cause) {
		super(cause);
	}

	public BadPyramidException(String message, Throwable cause) {
		super(message, cause);
	}
}
