package com.b2wdigital.pyramid.core;

import java.math.BigDecimal;

import org.apache.log4j.Logger;

import com.b2wdigital.pyramid.exception.BadPyramidException;

/**
 * HelloWord
 * 
 * @author Renanh Lima
 * @version 1.0
 * @since 2018-02-16
 */
public class Pyramid {

	private final static Logger logger = Logger.getLogger(Pyramid.class);
	private Integer[][] pyramidArray;

	public Pyramid(Integer[][] pyramidArray) {
		this.pyramidArray = pyramidArray;
	}
	
	/**
	 * Method responsible for validating if the array is valid (not empty or null) and if is Pyramid.
	 * 
	 * @return boolean
	 * @throws BadPyramidException
	 */
	protected boolean validatePyramid() throws BadPyramidException {
		if (this.pyramidArray == null)
			throw new NullPointerException("Pyramid array is null");
		for (int i = 0; i < pyramidArray.length; i++) {
			if (pyramidArray[i].length > (i + 1))
				throw new BadPyramidException("Wrong array dimension, not is a Pyramid");
		}

		return true;
	}

	/**
	 * Method responsible for finding maximum sum the Pyramid
	 * 
	 * @return boolean
	 * @throws BadPyramidException
	 */
	public long getMaximumTotal() throws BadPyramidException {
		validatePyramid();
		StringBuffer sumExposed = new StringBuffer();
		
		BigDecimal sum = new BigDecimal(pyramidArray[0][0]);
		int column = 0;

		sumExposed.append(pyramidArray[0][0]);

		logger.debug("Initializing Maximum Total calculation.");

		for (int i = 1; i < this.pyramidArray.length; i++) {
			Integer[] row = pyramidArray[i];
			int x = row[column];
			int y = row[column + 1];

			if (y > x) {
				sum = sum.add(new BigDecimal(y));
				column++;
				sumExposed.append(" + " + y);
			} else {
				sumExposed.append(" + " + x);
				sum = sum.add(new BigDecimal(x));
			}
		}

		sumExposed.append(" = " + sum.longValue());
		logger.debug("Maximum Total calculation ended.");
		logger.debug(sumExposed.toString());
		return sum.longValue();
	}

	public Object[][] getPyramidArray() {
		return pyramidArray;
	}
}
